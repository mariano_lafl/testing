
/*
	EVENT LISTENERS FOR VIDEO EVENTS
*/

	/* EVENT 1: video started */

function testVideo_start(category, label) {
	wistiaEmbed.bind("play", function () {
		_gaq.push(['_trackEvent', category, "Video Started", label]);
	});
}		

	/* EVENT 2: watched 25% (or the second at the 25% mark) */

function Video_25(category, label){
	wistiaEmbed.bind("secondchange", function (s) {
		var dur = wistiaEmbed.duration();
		console.log(s);
		if(s > dur/4 & s < (dur/4 + 2)) {
			_gaq.push(['_trackEvent', category, "Video watched 25%", label]);
			console.log("video 25 event fired");
		}
	});
}

function Video_half(category, label){
	wistiaEmbed.bind("secondchange", function (s) {
		var dur = wistiaEmbed.duration();
		if(s > dur/2 & s < (dur/2 + 2)) {
			_gaq.push(['_trackEvent', category, "Video watched 50%", label]);
			console.log("video half event fired");
		}
	});
}

function Video_75(category, label){
	wistiaEmbed.bind("secondchange", function (s) {
		var dur = wistiaEmbed.duration();
		if(s > dur * .75 & s < (dur * .75 + 2)) {
			console.log(dur);
			_gaq.push(['_trackEvent', category, "Video watched 75%", label]);
			console.log("video 75 event fired");
		}
	});
}

function testVideo_end(category, label) {
	wistiaEmbed.bind("end", function () {
		_gaq.push(['_trackEvent', category, "Video Ended", label]);
		console.log("video end event fired");
	});
}	

