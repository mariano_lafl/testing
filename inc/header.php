<!DOCTYPE html>

<html>
<head>
	<title><?php echo $pageTitle; ?></title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<script type="text/javascript" src="../js/ga.js"></script>
</head>
<body>

	<div class="container">
		<h1><?php echo $pageTitle; ?></h1>
	</div>