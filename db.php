<?php

try {
	$testdb = new PDO("mysql:host=localhost;dbname=test;", "root", "");
	var_dump($testdb);
} catch (Exception $e) {
	echo "Could not connect to the database. <br>";
	echo $e;
	exit;
}

echo "It worked! <br>";

$sql = "CREATE TABLE IF NOT EXISTS `testtable` (
 `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
 `email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

try {
	$count = $testdb->exec($sql);
	echo "Executed the following mySQL command:<br>" . $sql . "<br>";  
	echo $count . " rows were affected<br>";
}
catch (Exception $e) {
	echo "Could not execute sql commands: " . $e; 
}

echo "<br><br>";

$sql2 = "INSERT INTO `testtable` (`name`, `email`) VALUES ('Mark', 'mark.pryce@hotmail.com');";

try {
	$count = $testdb->exec($sql2);
	echo "Executed the following SQL command:<br>" . $sql2 . "<br>";
	echo $count . " rows were affected<br>";
}
catch (Exception $e) {
	echo "command not succesfully: " . $e; 
}


?>