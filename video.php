<?php
	$pageTitle = "Testing page for Video Events";
	include("inc/header.php");
?>

	<div class="container" id="video">
		<h2>Testimonial Video</h2>

		<div class="cen">
		<div id="wistia_1luc6moss9" class="wistia_embed" style="width:590px;height:332px;margin:0em auto;">
			<div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
				<meta itemprop="name" content="Peru For Less Reviews Collection 12 - Travelers Rave about us!" />
				<meta itemprop="duration" content="PT17M34S" />
				<meta itemprop="thumbnailUrl" content="https://embed-ssl.wistia.com/deliveries/81d2104421da01483d14a9db3ed8462b5d01cf63.bin" />
				<meta itemprop="contentURL" content="https://embed-ssl.wistia.com/deliveries/8ecd6d4541d7e355520e3a957f1fdbe049eeaee9.bin" />
				<meta itemprop="embedURL" content="https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04&autoPlay=false&banner=false&controlsVisibleOnLoad=false&customColor=949078&endVideoBehavior=default&fullscreenDisabled=true&hdUrl%5B2pass%5D=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=347519762&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fe10cd413758484c52b6ada05fb789c67c311bfd0.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=1054.0&playButtonVisible=true&showPlayButton=true&showPlaybar=true&showVolume=true&stillUrl=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F863921d45d2e21610838e8dad337c3713dab7866.jpg%3Fimage_crop_resized%3D590x332&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F8ecd6d4541d7e355520e3a957f1fdbe049eeaee9.bin" />
				<meta itemprop="uploadDate" content="2014-04-03T08:54:12Z" />
				<object id="wistia_1luc6moss9_seo" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" style="display:block;height:332px;position:absolute;width:590px;">
					<param name="movie" value="https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04"></param>
					<param name="allowfullscreen" value="true"></param>
					<param name="allowscriptaccess" value="always"></param>
					<param name="bgcolor" value="#000000"></param>
					<param name="wmode" value="opaque"></param>
					<param name="flashvars" value="autoPlay=false&banner=false&controlsVisibleOnLoad=false&customColor=949078&endVideoBehavior=default&fullscreenDisabled=true&hdUrl%5B2pass%5D=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=347519762&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fe10cd413758484c52b6ada05fb789c67c311bfd0.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=1054.0&playButtonVisible=true&showPlayButton=true&showPlaybar=true&showVolume=true&stillUrl=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F863921d45d2e21610838e8dad337c3713dab7866.jpg%3Fimage_crop_resized%3D590x332&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F8ecd6d4541d7e355520e3a957f1fdbe049eeaee9.bin"></param>
					<embed src="https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04" allowfullscreen="true" allowscriptaccess="always" bgcolor=#000000 flashvars="autoPlay=false&banner=false&controlsVisibleOnLoad=false&customColor=949078&endVideoBehavior=default&fullscreenDisabled=true&hdUrl%5B2pass%5D=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=347519762&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fe10cd413758484c52b6ada05fb789c67c311bfd0.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=1054.0&playButtonVisible=true&showPlayButton=true&showPlaybar=true&showVolume=true&stillUrl=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F863921d45d2e21610838e8dad337c3713dab7866.jpg%3Fimage_crop_resized%3D590x332&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F8ecd6d4541d7e355520e3a957f1fdbe049eeaee9.bin" name="wistia_1luc6moss9_html" style="display:block;height:100%;position:relative;width:100%;" type="application/x-shockwave-flash" wmode="opaque"></embed>
				</object>
				<noscript itemprop="description">Peru For Less Reviews Collection 12 - Travelers Rave about us!</noscript>
			</div>
		</div>
		
		<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js"></script>
		<script>
		wistiaEmbed = Wistia.embed("1luc6moss9");
		</script>
		<script charset="ISO-8859-1" src="//fast.wistia.com/embed/medias/1luc6moss9/metadata.js"></script>
	</div>

	<script type="text/javascript" src="js/videos.js"></script> 
	<script type="text/javascript">

		/*
			EVENT LISTENERS FOR VIDEO EVENTS
		*/
			/* EVENT 1: video started */

		var category = 'Testimonial Video';
		var label = 'funny business'
		testVideo_start(category, label);
		testVideo_end(category, label)

	</script>

	<button type="btn" class="btn btn-primary" onclick="buttonClick()">Facebook</button>
	<br>
	Please track this button as a conversion: 
	<br>
	<a class ="btn btn-primary" onclick="ga('send','pageview','conversion button')">Conversion!</a>
	
