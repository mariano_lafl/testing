<html>
<head>
	<title>Testing Page</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<script type="text/javascript" scr="js/ga.js"></script>
</head>
<body>
	
	<div class="testing">
		<h1>Testing Page</h1>
		<p>text to test some javascript code</p>
		<button onclick="myFunction()" id="demo">Test</button>
		<a href="#" onclick="_gaq.push(['_trackEvent', 'first category', 'an action', 'label']);">Test link<a>
		<script>
		
		function myFunction() {
			_gaq.push(['_trackEvent', 'another category', "another action", 'label']);
		}

		</script>

	</div>

	<!-- 
	Experience Imagination Video 
	id="wistia_so5juhb11c" 
	!-->
	
	<div id="showcase" class="testing">
		<div id="wistia_so5juhb11c" class="wistia_embed" style="width:940px;height:320px;background-color:#000000">
			<div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
				<meta itemprop="name" content="Experience Imagination: In South America" />
				<meta itemprop="duration" content="PT5M48S" />
				<meta itemprop="thumbnailUrl" content="http://embed.wistia.com/deliveries/6cd3c5f20d6fc7f68bb7b71db81dd54a176b8e22.bin" />
				<meta itemprop="contentURL" content="http://embed.wistia.com/deliveries/491802a13a997beccfc76472dfe736468ed0ba41.bin" />
				<meta itemprop="embedURL" content="http://embed.wistia.com/flash/embed_player_v2.0.swf?2013-07-03&autoPlay=false&banner=false&controlsVisibleOnLoad=false&customColor=949078&endVideoBehavior=default&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2Fe4c5b1a8e74cbed5b2a924c0b9f0135b275b3968.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=348.0&playButtonVisible=true&showPlayButton=true&showPlaybar=true&showVolume=true&stillUrl=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F6cd3c5f20d6fc7f68bb7b71db81dd54a176b8e22.jpg%3Fimage_crop_resized%3D569x320&unbufferedSeek=false&videoUrl=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F491802a13a997beccfc76472dfe736468ed0ba41.bin" />
				<meta itemprop="uploadDate" content="2013-06-24T20:39:35Z" />
				<object id="wistia_so5juhb11c_seo" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" style="display:block;height:320px;position:relative;width:569px;">
					<param name="movie" value="http://embed.wistia.com/flash/embed_player_v2.0.swf?2013-07-03"></param>
					<param name="allowfullscreen" value="true"></param>
					<param name="allowscriptaccess" value="always"></param>
					<param name="bgcolor" value="#000000"></param>
					<param name="wmode" value="opaque"></param>
					<param name="flashvars" value="autoPlay=false&banner=false&controlsVisibleOnLoad=false&customColor=949078&endVideoBehavior=default&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2Fe4c5b1a8e74cbed5b2a924c0b9f0135b275b3968.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=348.0&playButtonVisible=true&showPlayButton=true&showPlaybar=true&showVolume=true&stillUrl=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F6cd3c5f20d6fc7f68bb7b71db81dd54a176b8e22.jpg%3Fimage_crop_resized%3D569x320&unbufferedSeek=false&videoUrl=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F491802a13a997beccfc76472dfe736468ed0ba41.bin"></param>
					<embed src="http://embed.wistia.com/flash/embed_player_v2.0.swf?2013-07-03" allowfullscreen="true" allowscriptaccess="always" bgcolor=#000000 flashvars="autoPlay=false&banner=false&controlsVisibleOnLoad=false&customColor=949078&endVideoBehavior=default&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2Fe4c5b1a8e74cbed5b2a924c0b9f0135b275b3968.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=348.0&playButtonVisible=true&showPlayButton=true&showPlaybar=true&showVolume=true&stillUrl=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F6cd3c5f20d6fc7f68bb7b71db81dd54a176b8e22.jpg%3Fimage_crop_resized%3D569x320&unbufferedSeek=false&videoUrl=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F491802a13a997beccfc76472dfe736468ed0ba41.bin" name="wistia_so5juhb11c_html" style="display:block;height:100%;position:relative;width:100%;" type="application/x-shockwave-flash" wmode="opaque"></embed>
				</object>
				<noscript itemprop="description">Experience Imagination by Latin America For Less. Three friends on an incredible journey through Argentina, Chile, Bolivia, Uruguay, Paraguay, and Southern Brazil in their old and trusty Land Rover. This is their story. Filmed by: Clemens Kr�ger, Vincent Urban, Stefan Templer Edited by: Vincent Urban</noscript>
			</div>
		</div>
	</div>
	
<!-- THEIR VIDEO	!-->

	<div id="wistia_29b0fbf547" class="wistia_embed" style="width:640px;height:360px;" data-video-width="640" data-video-height="360">&nbsp;</div>
	<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js"></script>
	<script>
	wistiaEmbed = Wistia.embed("29b0fbf547", {
	  version: "v1",
	  videoWidth: 640,
	  videoHeight: 360,
	  playerColor: "688AAD"
	});
	</script>

	<script type="text/JavaScript">
	wistiaEmbed.bind("secondchange", function (s) {
	  if(s === 10) {
	    testVideo10sec();
	  }
	});
	</script>

<!--
	
	OUR VIDEO

	!-->
	
	<script>
	wistiaEmbed2 = Wistia.embed("so5juhb11c", {
	  version: "v1",
	  videoWidth: 940,
	  videoHeight: 320,
	  playerColor: "000000"
	});
	</script>

	<script type="text/JavaScript">
	wistiaEmbed2.bind("secondchange", function (s) {
	  if(s === 10) {
	    testVideo60sec();
	  }
	});
	</script>




	<div class="testing content">
		
		<p id="test1">A Paragraph.</p>
		<button type="button" onclick="testFunction()">Try it</button>
		<p><strong>Note:</strong> Testing the use of an external js file called "test.js".</p>
		<script src="js/test.js"></script>

	</div>


<!--


	<div class="testing content">
		
		<p id="testTextButton">Was the button event sent? Not yet</p>
		<button type="button" onclick="testEvent()">Test Event</button>
		<p>Testing the use of a button with the script immediately below it</p>

		<script>

			function testEvent() {
				_gaq.push(['_trackEvent', 'test events', "click", 'label']);
				var x = document.getElementById("testTextButton");
				x.innerHTML="Test Event Sent!";
			}

		</script>

	</div>



!-->



	<div class="testing content">
		
		<p id="testText2">Was the event sent? Not yet</p>
		<button type="button" onclick="testEvent()">Test Event</button>
		<script src="js/ga.js"></script>

	<script type="text/javascript">
	function testEvent() {
		_gaq.push(['_trackEvent', 'test events', "click", 'label']);
		var x = document.getElementById("testText2");
		x.innerHTML="Test completed Successfully!";
	}
	</script>
	
	</div>


	<div class="testing content">
		
		<p id="testTextVideo">Listening for Video event...</p>
		<button type="button" onclick="testVideo()">Test Event</button>
		<button type="button" onclick="testVideoReset()">Reset Event</button>
	
	<script type="text/javascript">
	function testVideo() {
		_gaq.push(['_trackEvent', 'test events', "click", 'label']);
		var x = document.getElementById("testTextVideo");
		x.innerHTML="Test completed Successfully!";
	}
	function testVideoReset() {
		var x = document.getElementById("testTextVideo");
		x.innerHTML="Listening for Video event...";
	}
	</script>
	</div>

	<div class="testing content">
		
		<p id="testTextVideo60sec">Listening for event "video 60 seconds in" ...</p>
		<button type="button" onclick="testVideo60sec()">Test Event</button>
		<button type="button" onclick="testVideoReset60sec()">Reset Event</button>
	
	<script type="text/javascript">
	function testVideo60sec() {
		_gaq.push(['_trackEvent', 'video event', "60 seconds into video", 'no label']);
		var x = document.getElementById("testTextVideo60sec");
		x.innerHTML="Test completed Successfully!";
	}
	function testVideoReset60sec() {
		var x = document.getElementById("testTextVideo60sec");
		x.innerHTML='Listening for event "video 60 seconds in" ...';
	}
	</script>
	</div>
<!---
	TESTING WITH THE WISTIA VIDEO
	!-->
	
	<div class="testing content">
		
		<p id="testTextVideo10sec">Listening for event "video 10 seconds in" ...</p>
		<button type="button" onclick="testVideo10sec()">Test Event</button>
		<button type="button" onclick="testVideoReset10sec()">Reset Event</button>
	
	<script type="text/javascript">
	function testVideo10sec() {
		_gaq.push(['_trackEvent', 'video event', "10 seconds into video", 'no label']);
		var x = document.getElementById("testTextVideo10sec");
		x.innerHTML="Test completed Successfully!";
	}
	function testVideoReset10sec() {
		var x = document.getElementById("testTextVideo10sec");
		x.innerHTML='Listening for event "video 10 seconds in" ...';
	}
	</script>
	</div>



</body>

</html>