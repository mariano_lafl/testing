<?php

/* save in the count variable the number of affected rows */
$sql = "CREATE TABLE IF NOT EXISTS `testtable` (
 `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
 `email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

$count = $testdb->exec($sql);
echo $count . " rows were affected";
?>

