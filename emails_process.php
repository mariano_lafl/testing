

<?php 
$pageTitle = "Testing page for Form Email"; 
include("inc/header.php");

try {
	$testdb = new PDO("mysql:host=localhost;dbname=test;", "root", "");
}
catch (Exception $e) {
	echo "Could not connect to the database: <br>";
	echo $e;
	exit; 
}

$randomKeyChars = "ABCDFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
$token_length = 20;
$token = "" ;
for ($i=0; $i < $token_length; $i++) { 
	$randomIndex = rand(0, (strlen($randomKeyChars) - 1));
	$token = $token . $randomKeyChars[$randomIndex];
}

?>
	<div class='container'>
		<p>
			<strong>Token:</strong>
		</p>
		<p id="token">
			<?php echo $token?>
		</p>
	</div>

<script type="text/javascript">

	var user_id = document.getElementById('token').innerHTML;
	_gaq.push(['_trackEvent', 'User ID', "form sent!", user_id]);
	console.log(user_id);

</script>

<?php 
$name = $_POST['name']; 
$email = $_POST['email'];

$sql = "INSERT INTO `testtable` (`name`, `email`, `user_id`) VALUES ('" . trim($name) . "', '" . trim($email) . "', '" . trim($token) . "');";

try {
	$count = $testdb->exec($sql);
	echo $count . " rows were affected<br>";
}
catch (Exception $e) {
	echo "execution unsuccessful: " . $e; 
}

?>
