CREATE DATABASE `testdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

USE `testdb`;

CREATE TABLE IF NOT EXISTS `products` (
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `testTable` (`name`, `email`) VALUES('Stephen', 'stephen.m.cabrera@gmail.com');

CREATE TABLE IF NOT EXISTS `testtable` (
 `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
 `email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

